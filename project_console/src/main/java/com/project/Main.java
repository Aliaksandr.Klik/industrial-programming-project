package com.project;

import com.library.*;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;
import java.nio.file.*;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter input file name or write . to exit: ");
        File inputFile = null;
        while (true) {
            String filename = input.nextLine();
            if (filename.equals(".")) {
                System.out.println("Exiting...");
                System.exit(0);
            }
            inputFile = new File(filename);
            if (inputFile.exists()) {
                break;
            } else {
                System.out.print("File not found. Try again: ");
            }
        }
        System.out.print("Enter input file format (txt, xml, or json): ");
        String format = input.nextLine();
        System.out.print("Use library implementation? (y/n): ");
        boolean library = false;
        while (true) {
            String answer = input.nextLine();
            if (answer.equals("y")) {
                library = true;
                break;
            } else if (answer.equals("n")) {
                library = false;
                break;
            } else {
                System.out.print("Invalid answer. Try again: ");
            }
        }

        String data = "";
        try {
            data = FileSupport.ReadFile(inputFile.getName());
        } catch (IOException e) {
            System.out.println("Error reading file");
            e.printStackTrace();
            System.exit(1);
        }

        ArrayList<Expression> expressions = switch (format) {
            case "txt" -> Expression.parseTxt(data, library);
            case "xml" -> Expression.parseXml(data, library);
            case "json" -> Expression.parseJson(data, library);
            default -> null;
        };

        if (expressions == null) {
            System.out.println("Invalid input file format");
            System.exit(1);
        }

        System.out.print("Enter output file name: ");
        String outputFilename = input.nextLine();
        System.out.print("Enter output file format (txt, xml, or json): ");
        String outputFormat = input.nextLine();

        String outputData = switch (outputFormat) {
            case "txt" -> Expression.createTxt(expressions);
            case "xml" -> Expression.createXml(expressions);
            case "json" -> Expression.createJson(expressions);
            default -> null;
        };

        if (outputData == null) {
            System.out.println("Invalid output file format");
            System.exit(1);
        }

        try {
            FileSupport.WriteFile(outputFilename, outputData);
        } catch (IOException e) {
            System.out.println("Error writing file");
            System.exit(1);
        }

        System.out.println("Done");

        input.close();

        System.exit(0);
    }
}