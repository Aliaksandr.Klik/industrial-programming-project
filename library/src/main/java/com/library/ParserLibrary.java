package com.library;

import net.objecthunter.exp4j.*;

public class ParserLibrary {
    public static Double evaluate(String expression) throws Exception {
        return new ExpressionBuilder(expression)
                .build()
                .evaluate();
    }
}
