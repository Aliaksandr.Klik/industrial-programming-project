package com.library;

import java.util.Stack;

import static java.lang.Character.*;

public class ParserStack {
    private static final String operators = "+-*/%^";

    protected static boolean isDigit(char ch) {
        return ch >= '0' && ch <= '9';
    }

    protected static boolean isOperator(char ch) {
        return operators.indexOf(ch) != -1;
    }

    protected static int precedence(char ch) {
        return switch (ch) {
            case '+', '-' -> 1;
            case '*', '/', '%' -> 2;
            case '^' -> 3;
            default -> -1;
        };
    }

    protected static Double applyOp(Double a, Double b, char op) {
        return switch (op) {
            case '+' -> a + b;
            case '-' -> a - b;
            case '*' -> a * b;
            case '/' -> a / b;
            case '%' -> a % b;
            case '^' -> Math.pow(a, b);
            default -> Double.NaN;
        };
    }

    public static Double evaluate(String expression) throws Exception {
        char[] tokens = expression.toCharArray();

        Stack<Double> values = new Stack<>();
        Stack<Character> ops = new Stack<>();

//        for (int i = 0; i < tokens.length; i++) {
//            if (isWhitespace(tokens[i])) {
//                continue;
//            }
//
//            if (isDigit(tokens[i])) {
//                StringBuilder sbuf = new StringBuilder();
//                while (i < tokens.length && (isDigit(tokens[i]) || tokens[i] == '.')) {
//                    sbuf.append(tokens[i++]);
//                }
//                values.push(Double.parseDouble(sbuf.toString()));
//                i--;
//                continue;
//            }
//
//            if (tokens[i] == '(') {
//                ops.push(tokens[i]);
//                continue;
//            }
//
//            if (tokens[i] == ')') {
//                while (ops.peek() != '(') {
//                    values.push(applyOp(values.pop(), values.pop(), ops.pop()));
//                }
//                ops.pop();
//                continue;
//            }
//
//            if (isOperator(tokens[i])) {
//                while (!ops.empty() && precedence(ops.peek()) >= precedence(tokens[i])) {
//                    values.push(applyOp(values.pop(), values.pop(), ops.pop()));
//                }
//                ops.push(tokens[i]);
//                continue;
//            }
//
//            if (!isDigit(tokens[i]) && !isOperator(tokens[i])) {
//                throw new Exception("Invalid character: " + tokens[i]);
//            }
//        }
//
//        while (!ops.empty()) {
//            values.push(applyOp(values.pop(), values.pop(), ops.pop()));
//        }
//
//        return values.pop();

        for (int i = tokens.length - 1; i >= 0; i--) {
            if (isWhitespace(tokens[i])) {
                continue;
            }

            if (isDigit(tokens[i])) {
                StringBuilder sbuf = new StringBuilder();
                while (i >= 0 && (isDigit(tokens[i]) || tokens[i] == '.')) {
                    sbuf.append(tokens[i--]);
                }
                values.push(Double.parseDouble(sbuf.reverse().toString()));
                i++;
                continue;
            }

            if (tokens[i] == ')') {
                ops.push(tokens[i]);
                continue;
            }

            if (tokens[i] == '(') {
                while (ops.peek() != ')') {
                    values.push(applyOp(values.pop(), values.pop(), ops.pop()));
                }
                ops.pop();
                continue;
            }

            if (isOperator(tokens[i])) {
                while (!ops.empty() && precedence(ops.peek()) > precedence(tokens[i])) {
                    values.push(applyOp(values.pop(), values.pop(), ops.pop()));
                }
                ops.push(tokens[i]);
                continue;
            }

            if (!isDigit(tokens[i]) && !isOperator(tokens[i])) {
                throw new Exception("Invalid character: " + tokens[i]);
            }
        }

        while (!ops.empty()) {
            values.push(applyOp(values.pop(), values.pop(), ops.pop()));
        }

        return values.pop();
    }
}
