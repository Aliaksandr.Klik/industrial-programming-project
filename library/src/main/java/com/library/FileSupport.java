package com.library;

import java.io.*;

public class FileSupport {
    public static boolean ValidFilename(String filename) throws IOException {
        File file = new File(filename);
        boolean created = false;
        try {
            created = file.createNewFile();
            return created;
        } finally {
            if (created) {
                file.delete();
            }
        }
    }

    public static String ReadFile(String filename) throws IOException {
        File file = new File(filename);
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder data = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            data.append(line);
            data.append("\n");
        }
        br.close();
        return data.toString();
    }

    public static void WriteFile(String filename, String data) throws IOException {
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(data);
        bw.close();
    }
}
