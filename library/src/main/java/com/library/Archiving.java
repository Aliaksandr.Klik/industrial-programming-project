package com.library;

import net.lingala.zip4j.*;
import net.lingala.zip4j.exception.*;
import net.lingala.zip4j.model.*;
import net.lingala.zip4j.model.enums.*;

import java.io.File;

public class Archiving {
    public static void zip(String source, String destination, String password) throws ZipException {
        ZipFile zipFile = new ZipFile(destination);
        ZipParameters parameters = new ZipParameters();
        parameters.setCompressionMethod(CompressionMethod.DEFLATE);
        parameters.setCompressionLevel(CompressionLevel.NORMAL);
        if (password != null) {
            parameters.setEncryptFiles(true);
            parameters.setEncryptionMethod(EncryptionMethod.AES);
            parameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
            zipFile.setPassword(password.toCharArray());
        }
        File file = new File(source);
        if (file.isFile()) {
            zipFile.addFile(file, parameters);
        } else if (file.isDirectory()) {
            zipFile.addFolder(file, parameters);
        }
    }

    public static void unzip(String source, String destination, String password) throws ZipException {
        ZipFile zipFile = new ZipFile(source);
        if (zipFile.isEncrypted()) {
            zipFile.setPassword(password.toCharArray());
        }
        zipFile.extractAll(destination);
    }
}
