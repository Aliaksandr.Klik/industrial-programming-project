package com.library;

import java.util.ArrayList;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.fasterxml.jackson.dataformat.xml.*;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import com.fasterxml.jackson.dataformat.xml.ser.*;
import com.fasterxml.jackson.dataformat.xml.deser.*;
import com.fasterxml.jackson.dataformat.xml.util.*;

public class Expression {
    private String expression;
    private Double result;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public Double getResult() {
        return result;
    }

    public Expression(String expression, boolean library) {
        this.expression = expression;
        try {
            if (library) {
                this.result = ParserLibrary.evaluate(expression);
            } else {
                this.result = ParserStack.evaluate(expression);
            }
        } catch (Exception e) {
            this.result = Double.NaN;
        }
    }

    @Override
    public String toString() {
        return "[" + expression + " = " + result + "]";
    }

    public static ArrayList<Expression> parseTxt(String data, boolean library) {
        ArrayList<Expression> expressions = new ArrayList<>();
        Stream<String> lines = data.lines();
        lines.forEach(line -> {
            expressions.add(new Expression(line, library));
        });
        return expressions;
    }

    public static String createTxt(ArrayList<Expression> expressions) {
        StringBuilder sb = new StringBuilder();
        expressions.forEach(expression -> {
            sb.append(expression.toString());
            sb.append("\n");
        });
        return sb.toString();
    }

    public static ArrayList<Expression> parseJson(String data, boolean library) {
        ArrayList<Expression> expressions = new ArrayList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(data);
            JsonNode expressionsNode = root.path("expressions");
            for (JsonNode expressionNode : expressionsNode) {
                expressions.add(new Expression(expressionNode.asText(), library));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return expressions;
    }

    public static String createJson(ArrayList<Expression> expressions) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode root = mapper.createObjectNode();
            ArrayNode expressionsNode = mapper.createArrayNode();
            expressions.forEach(expression -> {
                ObjectNode expressionNode = mapper.createObjectNode();
                expressionNode.put("expression", expression.getExpression());
                expressionNode.put("result", expression.getResult());
                expressionsNode.add(expressionNode);
            });
            root.set("expressions", expressionsNode);
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static ArrayList<Expression> parseXml(String data, boolean library) {
        ArrayList<Expression> expressions = new ArrayList<>();
        try {
            XmlMapper mapper = new XmlMapper();
            Expressions expressionsNode = mapper.readValue(data, Expressions.class);
            for (Expression expression : expressionsNode.expressions) {
                expressions.add(new Expression(expression.expression, library));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return expressions;
    }

    public static String createXml(ArrayList<Expression> expressions) {
        try {
            XmlMapper mapper = new XmlMapper();
            Expressions expressionsNode = new Expressions();
            expressions.forEach(expression -> {
                expressionsNode.expressions.add(new Expression(expression.getExpression(), false));
            });
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(expressionsNode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static class Expressions {
        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "expression")
        private ArrayList<Expression> expressions = new ArrayList<>();
    }
}
