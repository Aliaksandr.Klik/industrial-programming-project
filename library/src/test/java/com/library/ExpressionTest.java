package com.library;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionTest {

    @Test
    void parseTxt() {
        String data = """
                1
                1+1
                2+2*2
                (2+2)*2
                8/2*(2+2)
                1+2*3%4-4^5
                """;
        String expected = "[[1 = 1.0], [1+1 = 2.0], [2+2*2 = 6.0], [(2+2)*2 = 8.0], [8/2*(2+2) = 16.0], [1+2*3%4-4^5 = -1021.0]]";
        assertEquals(expected, Expression.parseTxt(data, true).toString().trim());
        assertEquals(expected, Expression.parseTxt(data, false).toString().trim());
    }

    @Test
    void createTxt() {
        String data = """
                1
                1+1
                2+2*2
                (2+2)*2
                8/2*(2+2)
                1+2*3%4-4^5
                """;
        String expected = """
                [1 = 1.0]
                [1+1 = 2.0]
                [2+2*2 = 6.0]
                [(2+2)*2 = 8.0]
                [8/2*(2+2) = 16.0]
                [1+2*3%4-4^5 = -1021.0]""";
        assertEquals(expected, Expression.createTxt(Expression.parseTxt(data, true)).trim());
        assertEquals(expected, Expression.createTxt(Expression.parseTxt(data, false)).trim());
    }

    @Test
    void parseJson() {
        String data = """
                {
                    "expressions": [
                        "1",
                        "1+1",
                        "2+2*2",
                        "(2+2)*2",
                        "8/2*(2+2)",
                        "1+2*3%4-4^5"
                    ]
                }""";
        String expected = "[[1 = 1.0], [1+1 = 2.0], [2+2*2 = 6.0], [(2+2)*2 = 8.0], [8/2*(2+2) = 16.0], [1+2*3%4-4^5 = -1021.0]]";
        assertEquals(expected, Expression.parseJson(data, true).toString().trim());
        assertEquals(expected, Expression.parseJson(data, false).toString().trim());
    }

    @Test
    void createJson() {
        String data = """
                {
                    "expressions": [
                        "1",
                        "1+1",
                        "2+2*2",
                        "(2+2)*2",
                        "8/2*(2+2)",
                        "1+2*3%4-4^5"
                    ]
                }""";
        String expected = """
{
  "expressions" : [ {
    "expression" : "1",
    "result" : 1.0
  }, {
    "expression" : "1+1",
    "result" : 2.0
  }, {
    "expression" : "2+2*2",
    "result" : 6.0
  }, {
    "expression" : "(2+2)*2",
    "result" : 8.0
  }, {
    "expression" : "8/2*(2+2)",
    "result" : 16.0
  }, {
    "expression" : "1+2*3%4-4^5",
    "result" : -1021.0
  } ]
}""";
        assertEquals(expected, Expression.createJson(Expression.parseJson(data, true)).trim());
        assertEquals(expected, Expression.createJson(Expression.parseJson(data, false)).trim());
    }

    @Test
    void parseXml() {
        String data = """
                <expressions>
                    <expression>1</expression>
                    <expression>1+1</expression>
                    <expression>2+2*2</expression>
                    <expression>(2+2)*2</expression>
                    <expression>8/2*(2+2)</expression>
                    <expression>1+2*3%4-4^5</expression>
                </expressions>""";
        String expected = "[[1 = 1.0], [1+1 = 2.0], [2+2*2 = 6.0], [(2+2)*2 = 8.0], [8/2*(2+2) = 16.0], [1+2*3%4-4^5 = -1021.0]]";
        assertEquals(expected, Expression.parseXml(data, true).toString().trim());
        assertEquals(expected, Expression.parseXml(data, false).toString().trim());
    }

    @Test
    void createXml() {
        String data = """
                <expressions>
                    <expression>1</expression>
                    <expression>1+1</expression>
                    <expression>2+2*2</expression>
                    <expression>(2+2)*2</expression>
                    <expression>8/2*(2+2)</expression>
                    <expression>1+2*3%4-4^5</expression>
                </expressions>""";
        String expected = """
                [1 = 1.0]
                [1+1 = 2.0]
                [2+2*2 = 6.0]
                [(2+2)*2 = 8.0]
                [8/2*(2+2) = 16.0]
                [1+2*3%4-4^5 = -1021.0]""";
        assertEquals(expected, Expression.createXml(Expression.parseXml(data, true)).trim());
        assertEquals(expected, Expression.createXml(Expression.parseXml(data, false)).trim());
    }
}