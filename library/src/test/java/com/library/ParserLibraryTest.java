package com.library;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserLibraryTest {
    static final Double EPSILON = 0.000001;

    @Test
    void evaluate() {
        try {
            assertEquals(1.0, ParserStack.evaluate("1"), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("1+1"), EPSILON);
            assertEquals(6.0, ParserStack.evaluate("2+2*2"), EPSILON);
            assertEquals(8.0, ParserStack.evaluate("(2+2)*2"), EPSILON);
            assertEquals(16.0, ParserStack.evaluate("8/2*(2+2)"), EPSILON);
            assertEquals(-503.0 / 3, ParserStack.evaluate("1+2*3%4-4^5/6"), EPSILON);
            assertEquals(Double.NaN, ParserStack.evaluate("0/0"));
            assertEquals(Double.NaN, ParserStack.evaluate("1%0"));
            assertEquals(1, ParserStack.evaluate("1^0"), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("""
                    1
                    +1
                                    
                                    
                    /1         *1
                    """), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("1\r\n\t+1"), EPSILON);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }
}