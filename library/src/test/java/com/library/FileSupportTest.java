package com.library;

import static org.junit.jupiter.api.Assertions.*;

class ValidFilenameTest {

    @org.junit.jupiter.api.Test
    void validFilename() {
        try {
            assertTrue(FileSupport.ValidFilename("test_file.txt"));
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }

    @org.junit.jupiter.api.Test
    void invalidFilename() {
        try {
            assertFalse(FileSupport.ValidFilename("."));
        } catch (Exception e) {
            fail("Exception thrown");
        }
    }
}