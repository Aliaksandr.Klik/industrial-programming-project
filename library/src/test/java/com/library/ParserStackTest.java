package com.library;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserStackTest {
    static final Double EPSILON = 0.000001;

    @Test
    void isDigitGood() {
        for (char ch = '0'; ch <= '9'; ch++)
            assertTrue(ParserStack.isDigit(ch));
    }

    @Test
    void isDigitBad() {
        assertFalse(ParserStack.isDigit('a'));
        assertFalse(ParserStack.isDigit(' '));
        assertFalse(ParserStack.isDigit('\0'));
        assertFalse(ParserStack.isDigit('\n'));
    }

    @Test
    void isOperatorGood() {
        for (char ch : "+-*/%^".toCharArray())
            assertTrue(ParserStack.isOperator(ch));
    }

    @Test
    void isOperatorBad() {
        assertFalse(ParserStack.isOperator('0'));
        assertFalse(ParserStack.isOperator('a'));
        assertFalse(ParserStack.isOperator(' '));
        assertFalse(ParserStack.isOperator('\0'));
        assertFalse(ParserStack.isOperator('\n'));
    }

    @Test
    void precedence() {
        assertTrue(ParserStack.precedence('+') < ParserStack.precedence('*'));
        assertTrue(ParserStack.precedence('-') < ParserStack.precedence('/'));
        assertTrue(ParserStack.precedence('*') < ParserStack.precedence('^'));
        assertTrue(ParserStack.precedence('%') < ParserStack.precedence('^'));
        assertEquals(-1, ParserStack.precedence('a'));
    }

    @Test
    void applyOp() {
        assertEquals(4.0, ParserStack.applyOp(2.0, 2.0, '+'), EPSILON);
        assertEquals(0.0, ParserStack.applyOp(2.0, 2.0, '-'), EPSILON);
        assertEquals(4.0, ParserStack.applyOp(2.0, 2.0, '*'), EPSILON);
        assertEquals(1.0, ParserStack.applyOp(2.0, 2.0, '/'), EPSILON);
        assertEquals(0.0, ParserStack.applyOp(2.0, 2.0, '%'), EPSILON);
        assertEquals(4.0, ParserStack.applyOp(2.0, 2.0, '^'), EPSILON);
        assertEquals(Double.NaN, ParserStack.applyOp(2.0, 2.0, 'a'), EPSILON);
    }

    @Test
    void evaluate() {
        try {
            assertEquals(1.0, ParserStack.evaluate("1"), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("1+1"), EPSILON);
            assertEquals(6.0, ParserStack.evaluate("2+2*2"), EPSILON);
            assertEquals(8.0, ParserStack.evaluate("(2+2)*2"), EPSILON);
            assertEquals(16.0, ParserStack.evaluate("8/2*(2+2)"), EPSILON);
            assertEquals(-503.0 / 3, ParserStack.evaluate("1+2*3%4-4^5/6"), EPSILON);
            assertEquals(Double.NaN, ParserStack.evaluate("0/0"));
            assertEquals(Double.NaN, ParserStack.evaluate("1%0"));
            assertEquals(1, ParserStack.evaluate("1^0"), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("""
                    1
                    +1
                                    
                                    
                    /1         *1
                    """), EPSILON);
            assertEquals(2.0, ParserStack.evaluate("1\r\n\t+1"), EPSILON);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Exception thrown");
        }
    }
}