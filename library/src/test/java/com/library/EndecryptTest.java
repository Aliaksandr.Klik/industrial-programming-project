package com.library;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EndecryptTest {

    @Test
    void endecrypt() {
        String data = "Hello World!";
        String key = "1234567890";
        assertEquals(data, Endecrypt.endecrypt(Endecrypt.endecrypt(data, key), key));
    }
}